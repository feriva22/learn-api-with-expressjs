# api with expressjs

simple CRUD in express js

## Library use in this program :
1. Express JS with EJS view engine
2. Sequelize ORM
3. Postgresql Library (pg pg-hstore)
4. Swagger UI

## How to use:
1. execute npm install
2. set the database credential in config/config.json 
3. Migration database with npx sequelize db:migration
4. Run program with npm start