const department = require('../models/department');

const User = require('../models').User;

module.exports = {
    list(req, res) {
      return Department
        .findAll({
          include: [],
          order: [
            ['createdAt', 'DESC'],
          ],
        })
        .then((departments) => res.status(200).send(departments))
        .catch((error) => { res.status(400).send(error); });
    },

    getById(req, res) {
        return Department
            .findByPk(req.params.id, {
                include: [],
            })
            .then((department) => {
                if (!department) {
                    return res.status(404).send({
                        message: 'Departments Not Found',
                    });
                }
                return res.status(200).send(department);
            })
            .catch((error) => res.status(400).send(error));
    },
    add(req, res) {
        return Department
            .create({
                id: req.body.id,
                name: req.body.nameDept,
            })
            .then((department) => res.status(201).send(department))
            .catch((error) => res.status(400).send(error));
    },
    update(req, res) {
        return Department
            .findByPk(req.params.id)
            .then(department => {
                if (!department) {
                    return res.status(404).send({
                        message: 'Department Not Found',
                    });
                }
                return department
                    .update({
                        name: req.body.nameDept || department.nameDept,
                    })
                    .then(() => res.status(200).send(department))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    delete(req, res) {
        return Department
            .findByPk(req.params.id)
            .then(department => {
                if (!department) {
                    return res.status(400).send({
                        message: 'Department Not Found',
                    });
                }
                return department
                    .destroy()
                    .then(() => res.status(204).send())
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },
}  