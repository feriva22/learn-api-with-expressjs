var express = require('express');
var router = express.Router();

const userController = require('../controllers').user;
const departmentController = require('../controllers').department;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Data API' });
});

/* User Router */
router.get('/api/user', userController.list);
router.get('/api/user/:id', userController.getById);
router.post('/api/user', userController.add);
router.put('/api/user/:id', userController.update);
router.delete('/api/user/:id', userController.delete);

/* Department Router */
router.get('/api/department', departmentController.list);
router.get('/api/department/:id', departmentController.getById);
router.post('/api/department', departmentController.add);
router.put('/api/department/:id', departmentController.update);
router.delete('/api/department/:id', departmentController.delete);

module.exports = router;
